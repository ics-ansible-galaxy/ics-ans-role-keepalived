# ics-ans-role-keepalived

Ansible role to install keepalived.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-keepalived
```

## License

BSD 2-clause
